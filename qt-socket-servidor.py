# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'servidor.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import*
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import time

import connect1
import json

from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import base64
import zlib

import socket,pickle
import sys
archivo=""
llave_publica=""
llave_privada=""

HOST = '127.0.0.1'
PORT = 65432

class Ui_MainWindow(object):
    global llave_publica
    global llave_privada
    global archivo
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(325, 423)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setGeometry(QtCore.QRect(20, 30, 281, 31))
        self.textEdit.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.textEdit.setLineWidth(1)
        self.textEdit.setMidLineWidth(0)
        self.textEdit.setReadOnly(True)
        self.textEdit.setObjectName("textEdit")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(140, 10, 47, 13))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(20, 120, 47, 13))
        self.label_2.setObjectName("label_2")

        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(90, 80, 131, 23))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.seleccionarLlavePublica)

        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(90, 170, 131, 23))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(self.seleccionarLlavePrivada)

        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(20, 210, 47, 13))
        self.label_3.setObjectName("label_3")
        self.textEdit_2 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_2.setGeometry(QtCore.QRect(70, 110, 241, 31))
        self.textEdit_2.setReadOnly(True)
        self.textEdit_2.setObjectName("textEdit_2")
        self.textEdit_3 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_3.setGeometry(QtCore.QRect(70, 200, 241, 31))
        self.textEdit_3.setReadOnly(True)
        self.textEdit_3.setObjectName("textEdit_3")
        self.textEdit_4 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_4.setGeometry(QtCore.QRect(70, 270, 191, 71))
        self.textEdit_4.setReadOnly(True)
        self.textEdit_4.setObjectName("textEdit_4")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(130, 250, 71, 16))
        self.label_4.setObjectName("label_4")

        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(130, 350, 75, 23))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(self.recibir)
        
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 325, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Socket - Servidor"))
        self.label.setText(_translate("MainWindow", "Consola"))
        self.label_2.setText(_translate("MainWindow", "Ubicación:"))
        self.pushButton.setText(_translate("MainWindow", "Seleccionar Llave Publica"))
        self.pushButton_2.setText(_translate("MainWindow", "Seleccionar Llave Privada"))
        self.label_3.setText(_translate("MainWindow", "Ubicación:"))
        self.label_4.setText(_translate("MainWindow", "Dato Recibido"))
        self.pushButton_3.setText(_translate("MainWindow", "Recibir Dato"))

    def seleccionarLlavePrivada(self):
        fileName = QFileDialog.getOpenFileName(None, '\nSelecciona archivo permitido', '', '*.pem')
        private_key=str(fileName[0])
        print(private_key)
        self.textEdit_3.setText(private_key)
        self.textEdit.setText("\nLlave privada seleccionada: "+private_key)
    
    def seleccionarLlavePublica(self):
        fileName = QFileDialog.getOpenFileName(None, '\nSelecciona archivo permitido', '', '*.pem')
        public_key=str(fileName[0])
        print(public_key)
        self.textEdit_2.setText(public_key)
        self.textEdit.setText("\nLlave publica seleccionada: "+public_key)

    def recibir(self):
        conexion=connect1.connect1()

        if not self.textEdit_2.toPlainText() or not self.textEdit_3.toPlainText():
            self.textEdit.setText("Los campos con una * son obligatorios, favor de llenarlos")
        else:
            

            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.bind((HOST, PORT))
                s.listen()
                conn, addr = s.accept()
                with conn:
                    print('\nIP recibida: ', addr)
                    while True:
                        data = conn.recv(4096)
                        if not data:
                            break
                        
                        dato=pickle.loads(data)
                        datojson=json.loads(dato)
                        desencriptado=conexion.decrypt_blob(datojson["encripcion"].encode(),datojson["llavePrivada"].encode())
                        verdad=conexion.verify_sign(datojson["llavePublica"].encode(),datojson["firma"].encode(),desencriptado)
                        if verdad==False:
                            print("Falso")
                            break
                        desencriptado=desencriptado.decode(encoding='UTF-8',errors='strict')
                        self.textEdit_4.setText(desencriptado)
                        self.textEdit.setText("Dato Recibido: "+desencriptado)
                        fd = open(self.textEdit_2.toPlainText(), "rb")
                        llave = fd.read()
                        fd.close()
                        print(2)
                        fd = open(self.textEdit_3.toPlainText(), "rb")
                        llave2 = fd.read()
                        fd.close()
                        print(3)
                        encrypted_blob=conexion.encrypt_blob(str.encode(desencriptado),llave)
                        d=conexion.sign_data(llave2,str.encode(desencriptado))

                        firma=d.decode(encoding='UTF-8',errors='strict')
                        encripcion=encrypted_blob.decode(encoding='UTF-8',errors='strict')
                        llavePublica=llave.decode(encoding='UTF-8',errors='strict')
                        llavePrivada=llave2.decode(encoding='UTF-8',errors='strict')
                        y={
                            "firma": firma,
                            "encripcion": encripcion,
                            "llavePublica":llavePublica,
                            "llavePrivada":llavePrivada
                        }
                        print(5)
                        try:
                            p=json.load(y)
                            print(p)
                        except:
                            print("error")    
                        print(6)
                        asd=json.dumps(y)
                        print(7)
                        v=pickle.dumps(asd)
                        conn.sendall(v)
                        

            
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())