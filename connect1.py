from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256 
import zlib
import base64

class connect1:
    
    def sign_data(self,private_key, data):
        '''
        param: private_key Llave privada
        param: data Data a firmar
        return: Firma codificado a base64 
        '''

        rsakey = RSA.importKey(private_key) 
        signer = PKCS1_v1_5.new(rsakey) 
        digest = SHA256.new() 
    
        digest.update(base64.b64encode(data)) 
        sign = signer.sign(digest) 
        return base64.b64encode(sign)

    def verify_sign(self,public_key, signature, data):
        '''
        param: public_key Llave publica
        param: signature Firma
        param: data archivo originario de la firma
        return: Archivo decodificado
        '''
    
        rsakey = RSA.importKey(public_key) 
        signer = PKCS1_v1_5.new(rsakey) 
        digest = SHA256.new() 
        # Assumes the data is base64 encoded to begin with
        digest.update(base64.b64encode(data)) 
        if signer.verify(digest, base64.b64decode(signature)):
            return True
        return False

    def decrypt_blob(self,encrypted_blob, private_key):
        '''
        param: encrypted_blob dato encriptado para su desencriptación
        param: private_key  Llave privada
        return: Archivo decodificado
        '''

    #Importar la Llave privada correspondiente para desencriptar
        rsakey = RSA.importKey(private_key)
        rsakey = PKCS1_OAEP.new(rsakey)

    #Decodificacion de Base64
        encrypted_blob = base64.b64decode(encrypted_blob)

    #determinado por el tamaño del chunk, determinar la longitud en bites con la llave privada en Bytes
    #El dato sera desencriptado en chunks
        chunk_size = 1024
        offset = 0
        decrypted = ""

    #El loop seguirá hasta terminar el chunk
        while offset < len(encrypted_blob):
        #El chunk
            chunk = encrypted_blob[offset: offset + chunk_size]

        #Añadir los datos al valor para su desencriptación
            decrypted = bytearray(decrypted, 'utf-8')+ rsakey.decrypt(chunk)
            offset += chunk_size

    #Retornar el valor descomprimido 
        return zlib.decompress(decrypted)



    def encrypt_blob(self,blob, public_key):
        '''
        param: blob dato a encriptar
        param: public_key llave publica
        return: base64 codificado
        '''
    #Importar la llave Publica y usarla para encriptacion usandp PKCS1_OAEP
        rsa_key = RSA.importKey(public_key)
        rsa_key = PKCS1_OAEP.new(rsa_key)

    #comprimir los datos 
        blob = zlib.compress(blob)


    #determinado por el tamaño del chunk, determinar la longitud en bites con la llave publica en Bytes
    # y substraer en 42 b (mientras se usa PKCS1_OAEP). la info va a ser encriptada
    #in chunks
        chunk_size = 210
        offset = 0
        end_loop = False
        encrypted =  ""

        while not end_loop:
        #El chunk
            chunk = blob[offset:offset + chunk_size]

        #si la longitud de la info del chunk menor que el tamaño del chunk, entonces se añade
        #concatenando b" ". esto indica que es el final del archivo

            if len(chunk) % chunk_size != 0:
                end_loop = True
                chunk += b" " * (chunk_size - len(chunk))

            encrypted = bytearray(encrypted, 'utf-8')+rsa_key.encrypt(chunk)

            offset += chunk_size

    #usando Base 64 para encriptar de manera segura el archivo


        return base64.b64encode(encrypted)
    

    