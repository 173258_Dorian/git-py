#ch9_encrypt_blob.py
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import zlib
import base64

# Función de encriptación
def encrypt_blob(blob, public_key):
    print(blob)
    #Importar la llave Publica y usarla para encriptacion usandp PKCS1_OAEP
    rsa_key = RSA.importKey(public_key)
    rsa_key = PKCS1_OAEP.new(rsa_key)

    #comprimir los datos 
    blob = zlib.compress(blob)
    print(blob)


    #determinado por el tamaño del chunk, determinar la longitud en bites con la llave primaria
    # y substraer en 42 b (mientras se usa PKCS1_OAEP). la info va a ser encriptada
    #in chunks
    chunk_size = 210
    offset = 0
    end_loop = False
    encrypted =  ""

    while not end_loop:
        #El chunk
        chunk = blob[offset:offset + chunk_size]

        #si la longitud de la info del chunk menor que el tamaño del chunk, entonces se añade
        #concatenando b" ". esto indica que es el final del archivo

        if len(chunk) % chunk_size != 0:
            end_loop = True
            chunk += b" " * (chunk_size - len(chunk))

        encrypted = bytearray(encrypted, 'utf-8')+rsa_key.encrypt(chunk)

        offset += chunk_size

    #usando Base 64 para encriptar de manera segura el archivo


    return base64.b64encode(encrypted)


#Llave publica para encriptar
fd = open("publicKeySender.pem", "rb")
public_key = fd.read()
fd.close()

#nuestro archivo a encriptar
fd = open("texto.txt", "rb")
unencrypted_blob = fd.read()
print(unencrypted_blob)
fd.close()

encrypted_blob = encrypt_blob(unencrypted_blob, public_key)

#archivo ya encriptado
fd = open("encrypted_txt.txt", "wb")
fd.write(encrypted_blob)
fd.close()