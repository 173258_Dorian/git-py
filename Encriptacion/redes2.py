# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'redes1.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import*
from PyQt5.QtCore import *
from PyQt5.QtGui import *


from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import base64
import zlib

import sys
archivo=""
llave_publica=""
llave_privada=""
class Ui_MainWindow(object):



    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(509, 455)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(200, 240, 91, 31))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.enviarTexto)

        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(10, 150, 91, 31))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(self.encriptarTexto)

        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(10, 220, 91, 31))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(self.desencriptarTexto)

        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(110, 80, 101, 31))
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_4.clicked.connect(self.seleccionarLlavePublica)

        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setGeometry(QtCore.QRect(110, 120, 101, 31))
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_5.clicked.connect(self.seleccionarLlavePrivada)

        self.textEdit_2 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_2.setGeometry(QtCore.QRect(280, 80, 171, 31))
        self.textEdit_2.setReadOnly(True)
        self.textEdit_2.setObjectName("textEdit_2")

        self.textEdit_3 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_3.setGeometry(QtCore.QRect(280, 120, 171, 31))
        self.textEdit_3.setReadOnly(True)
        self.textEdit_3.setObjectName("textEdit_3")

        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(220, 90, 47, 13))
        self.label.setObjectName("label")

        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(220, 130, 47, 13))
        self.label_2.setObjectName("label_2")

        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(30, 270, 91, 16))
        self.label_3.setObjectName("label_3")

        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setGeometry(QtCore.QRect(10, 300, 131, 111))
        self.textEdit.setObjectName("textEdit")

        self.textEdit_4 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_4.setGeometry(QtCore.QRect(180, 300, 131, 111))
        self.textEdit_4.setReadOnly(True)
        self.textEdit_4.setObjectName("textEdit_4")

        self.textEdit_5 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_5.setGeometry(QtCore.QRect(350, 300, 131, 111))
        self.textEdit_5.setReadOnly(True)
        self.textEdit_5.setObjectName("textEdit_5")

        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(200, 270, 91, 16))
        self.label_4.setObjectName("label_4")

        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(360, 270, 111, 16))
        self.label_5.setObjectName("label_5")

        self.pushButton_6 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_6.setGeometry(QtCore.QRect(110, 160, 121, 31))
        self.pushButton_6.setObjectName("pushButton_6")
        self.pushButton_6.clicked.connect(self.seleccionarTextoParaEncriptar)
        

        self.pushButton_7 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_7.setGeometry(QtCore.QRect(110, 200, 141, 31))
        self.pushButton_7.setObjectName("pushButton_7")
        self.pushButton_7.clicked.connect(self.seleccionarTextoEncriptado)

        self.textEdit_6 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_6.setGeometry(QtCore.QRect(310, 160, 171, 31))
        self.textEdit_6.setReadOnly(True)
        self.textEdit_6.setObjectName("textEdit_6")

        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(250, 170, 47, 13))
        self.label_6.setObjectName("label_6")

        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setGeometry(QtCore.QRect(260, 210, 47, 13))
        self.label_7.setObjectName("label_7")

        self.textEdit_7 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_7.setGeometry(QtCore.QRect(320, 200, 171, 31))
        self.textEdit_7.setReadOnly(True)
        self.textEdit_7.setObjectName("textEdit_7")

        self.textEdit_8 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_8.setGeometry(QtCore.QRect(10, 30, 491, 41))
        self.textEdit_8.setReadOnly(True)
        self.textEdit_8.setObjectName("textEdit_8")
        self.textEdit_8.setAlignment(QtCore.Qt.AlignCenter)

        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setGeometry(QtCore.QRect(230, 10, 47, 13))
        self.label_8.setObjectName("label_8")

        MainWindow.setCentralWidget(self.centralwidget)
        
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 509, 21))
        self.menubar.setObjectName("menubar")
        self.menuMain = QtWidgets.QMenu(self.menubar)
        self.menuMain.setObjectName("menuMain")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.menubar.addAction(self.menuMain.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton.setText(_translate("MainWindow", "Enviar"))
        self.pushButton_2.setText(_translate("MainWindow", "Encriptar"))
        self.pushButton_3.setText(_translate("MainWindow", "Desencriptar"))
        self.pushButton_4.setText(_translate("MainWindow", "Elegir llave publica"))
        self.pushButton_5.setText(_translate("MainWindow", "Elegir llave privada"))
        self.label.setText(_translate("MainWindow", "Ubicación: "))
        self.label_2.setText(_translate("MainWindow", "Ubicación: "))
        self.label_3.setText(_translate("MainWindow", "* Texto a encriptar:"))
        self.label_4.setText(_translate("MainWindow", "Texto Encriptado: "))
        self.label_5.setText(_translate("MainWindow", "Texto desencriptado:"))
        self.pushButton_6.setText(_translate("MainWindow", "Abrir Texto a encriptar"))
        self.pushButton_7.setText(_translate("MainWindow", "Abrir texto ya encriptado"))
        self.label_6.setText(_translate("MainWindow", "Ubicación: "))
        self.label_7.setText(_translate("MainWindow", "Ubicación: "))
        self.label_8.setText(_translate("MainWindow", "Consola"))
        self.menuMain.setTitle(_translate("MainWindow", "Main"))

    def enviarTexto(self):
        print()
    
    def generarLlaves(self):
        
        try:
            new_key = RSA.generate(4096, e=65537)
            private_key = new_key.exportKey("PEM")
            public_key = new_key.publickey().exportKey("PEM")
            fd = open("private_key.pem", "wb")
            fd.write(private_key)
            fd.close()
            fd = open("public_key.pem", "wb")
            fd.write(public_key)
            fd.close()
        except:
            print("Llaves generadas incorrectamente, intente otra vez")
            self.textEdit_8.setText("\nLlaves generadas incorrectamente, intente otra vez")
        else:
            print("Llaves generadas correctamente")
            self.textEdit_8.setText("\nLlaves generadas correctamente")
    
    def seleccionarLlavePublica(self):
        fileName = QFileDialog.getOpenFileName(None, '\nSelecciona archivo permitido', '', '*.pem')
        public_key=str(fileName[0])
        print(public_key)
        self.textEdit_2.setText(public_key)
        self.textEdit_8.setText("\nLlave publica seleccionada: "+public_key)
    
    def seleccionarLlavePrivada(self):
        fileName = QFileDialog.getOpenFileName(None, '\nSelecciona archivo permitido', '', '*.pem')
        private_key=str(fileName[0])
        self.textEdit_3.setText(private_key)
        self.textEdit_8.setText("\nLlave privada seleccionada: "+private_key)

    def seleccionarTextoParaEncriptar(self):
        fileName = QFileDialog.getOpenFileName(None, '\nSelecciona archivo permitido', '', '*.txt')
        archivo=open(str(fileName[0]),'r').read()
        self.textEdit.setText(str(archivo))
        self.textEdit_8.setText("\nArchivo txt seleccionado a encriptar: "+str(archivo))
    
    def seleccionarTextoEncriptado(self):
        fileName = QFileDialog.getOpenFileName(None, '\nSelecciona archivo permitido', '', '*.txt')
        archivo=open(str(fileName[0]),'r').read()
        self.textEdit_4.setText(str(archivo))
        self.textEdit_8.setText("\nArchivo txt seleccionado a desencriptar: "+str(archivo))

    def desencriptarTexto(self):
        chunk_size = 512
        offset = 0
        decrypted = ""
        archivo=self.textEdit_4.toPlainText()
        llave_privada=self.textEdit_3.toPlainText()
        print(llave_privada)
        texto=self.textEdit_4.toPlainText()
        if(llave_privada==''):
            self.textEdit_8.setText("\nNo se ha seleccionado una llave publica/privada, favor de seleccionar ambos")
        else:
            if(texto==''):
                self.textEdit_8.setText("\nEl texto a encriptar esta vacio, favor de seleccionar o crear uno para su desencriptación.")
            else:
                try:
                    fd = open(llave_privada, "rb")
                    private_key = fd.read()
                    fd.close()
                    encrypted_blob = archivo

                    rsakey = RSA.importKey(private_key)
                    rsakey = PKCS1_OAEP.new(rsakey)

                
                    print("\n---------------------------")
                    encrypted_blob = base64.b64decode(encrypted_blob)
                    print(encrypted_blob)
                    print("\n---------------------------")
                    print(str.encode(encrypted_blob))
                    while offset < len(encrypted_blob):

                        chunk = encrypted_blob[offset: offset + chunk_size]

                        decrypted = bytearray(decrypted, 'utf-8')+ rsakey.decrypt(chunk)

                        offset += chunk_size
                    fd = open("decrypted_txt.txt", "wb")
                    desencriptado=zlib.decompress(decrypted)
                    print("\n---------------------------")
                    print(desencriptado)
                    fd.write(desencriptado)
                    fd.close()
                except:
                    print("error")
                else:
                    
                    
                    self.textEdit_5.setText(str(desencriptado))
                    


    def encriptarTexto(self):
        chunk_size = 470
        offset = 0
        end_loop = False
        encrypted =  ""
        texto=self.textEdit.toPlainText()
        llave_publica=self.textEdit_2.toPlainText()
        if(llave_publica==''):
            self.textEdit_8.setText("\nNo se ha seleccionado una llave publica/privada, favor de seleccionar ambos")
            
        else:
            if(texto==''):
                self.textEdit_8.setText("\nRellenar el campo requerido con la marca '*'")
            else:
                try:
                    fd = open(llave_publica, "rb")
                    llave = fd.read()
                    fd.close()
                    print(llave)

                    blob = str.encode(texto)
                    print(blob)

                    rsa_key = RSA.importKey(llave)
                    rsa_key = PKCS1_OAEP.new(rsa_key)
                    blob = zlib.compress(blob)
                    print(blob)
                    while not end_loop:

                        chunk = blob[offset:offset + chunk_size]

                        if len(chunk) % chunk_size != 0:
                            end_loop = True
                            chunk += b" " * (chunk_size - len(chunk))

                        encrypted = bytearray(encrypted, 'utf-8')+rsa_key.encrypt(chunk)

                        offset += chunk_size

    #usando Base 64 para encriptar de manera segura el archivo

                    encrypted_blob= base64.b64encode(encrypted)


#archivo ya encriptado

                    fd = open("encrypted_txt.txt", "wb")
                    fd.write(encrypted_blob)
                    fd.close()
                except:
                    print("error")
                else:
                    archivo=encrypted_blob
                    self.textEdit_4.setText(str(encrypted_blob))
                    self.textEdit_8.setText("\nArchivo encriptado.")

                

            






    def decrypt_blob(self, encrypted_blob, private_key):

    #Import the Private Key and use for decryption using PKCS1_OAEP
        rsakey = RSA.importKey(private_key)
        rsakey = PKCS1_OAEP.new(rsakey)

    #Base 64 decode the data
        encrypted_blob = base64.b64decode(encrypted_blob)

    #In determining the chunk size, determine the private key length used in bytes.
    #The data will be in decrypted in chunks
        chunk_size = 512
        offset = 0
        decrypted = ""

    #keep loop going as long as we have chunks to decrypt
        while offset < len(encrypted_blob):
        #The chunk
            chunk = encrypted_blob[offset: offset + chunk_size]

        #Append the decrypted chunk to the overall decrypted file
            decrypted = bytearray(decrypted, 'utf-8')+ rsakey.decrypt(chunk)

        #Increase the offset by chunk size
            offset += chunk_size

    #return the decompressed decrypted data
        return zlib.decompress(decrypted)

    def encrypt_blob(self,blob, public_key):

    #Importar la llave Publica y usarla para encriptacion usandp PKCS1_OAEP
        rsa_key = RSA.importKey(public_key)
        rsa_key = PKCS1_OAEP.new(rsa_key)

    #comprimir los datos en un blob
        blob = zlib.compress(blob)

    #determinado por el tamaño del chunk, determinar la longitud en bites con la llave primaria
    # y substraer en 42 b (mientras se usa PKCS1_OAEP). la info va a ser encriptada
    #in chunks
        chunk_size = 470
        offset = 0
        end_loop = False
        encrypted =  ""

        while not end_loop:
        #El chunk
            chunk = blob[offset:offset + chunk_size]

        #si la longitud de la info del chunk menor que el tamaño del chunk, entonces se añade
        #concatenando b" ". esto indica que es el final del archivo

            if len(chunk) % chunk_size != 0:
                end_loop = True
                chunk += b" " * (chunk_size - len(chunk))

            encrypted = bytearray(encrypted, 'utf-8')+rsa_key.encrypt(chunk)

            offset += chunk_size

    #usando Base 64 para encriptar de manera segura el archivo
        print(encrypted)

        return base64.b64encode(encrypted)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
