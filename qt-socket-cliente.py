# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'cliente.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import*
from PyQt5.QtCore import *
from PyQt5.QtGui import *

import connect1

import json

from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256 

import base64


import zlib
import socket, pickle

import sys
archivo=""
llave_publica=""
llave_privada=""

HOST = '127.0.0.1'
PORT = 65432


class Ui_MainWindow(object):


    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(402, 447)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setGeometry(QtCore.QRect(20, 30, 351, 31))
        self.textEdit.setReadOnly(True)
        self.textEdit.setObjectName("textEdit")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(170, 10, 47, 13))
        self.label.setObjectName("label")

        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(120, 70, 131, 23))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.seleccionarLlavePublica)

        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(120, 150, 131, 23))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(self.seleccionarLlavePrivada)


        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(20, 110, 47, 13))
        self.label_3.setObjectName("label_3")
        self.textEdit_2 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_2.setGeometry(QtCore.QRect(70, 100, 271, 31))
        self.textEdit_2.setReadOnly(True)
        self.textEdit_2.setObjectName("textEdit_2")
        self.textEdit_3 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_3.setGeometry(QtCore.QRect(70, 180, 271, 31))
        self.textEdit_3.setReadOnly(True)
        self.textEdit_3.setObjectName("textEdit_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(20, 190, 47, 13))
        self.label_4.setObjectName("label_4")

        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(140, 380, 81, 23))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(self.enviar)

        self.textEdit_4 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_4.setGeometry(QtCore.QRect(10, 260, 131, 101))
        self.textEdit_4.setObjectName("textEdit_4")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(40, 240, 71, 16))
        self.label_2.setObjectName("label_2")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(250, 240, 81, 16))
        self.label_5.setObjectName("label_5")
        self.textEdit_5 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_5.setGeometry(QtCore.QRect(220, 260, 131, 101))
        self.textEdit_5.setReadOnly(True)
        self.textEdit_5.setObjectName("textEdit_5")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 402, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Socket - Cliente"))
        self.label.setText(_translate("MainWindow", "Consola"))
        self.pushButton.setText(_translate("MainWindow", "Seleccionar Llave Publica"))
        self.pushButton_2.setText(_translate("MainWindow", "Seleccionar Llave Privada"))
        self.label_3.setText(_translate("MainWindow", "Ubicación *:"))
        self.label_4.setText(_translate("MainWindow", "Ubicación *:"))
        self.pushButton_3.setText(_translate("MainWindow", "Enviar"))
        self.label_2.setText(_translate("MainWindow", "Dato a enviar *:"))
        self.label_5.setText(_translate("MainWindow", "Dato Recibido:"))

    def seleccionarLlavePrivada(self):
        fileName = QFileDialog.getOpenFileName(None, '\nSelecciona archivo permitido', '', '*.pem')
        private_key=str(fileName[0])
        print(private_key)
        self.textEdit_3.setText(private_key)
        self.textEdit.setText("\nLlave privada seleccionada: "+private_key)
    
    def seleccionarLlavePublica(self):
        fileName = QFileDialog.getOpenFileName(None, '\nSelecciona archivo permitido', '', '*.pem')
        public_key=str(fileName[0])
        print(public_key)
        self.textEdit_2.setText(public_key)
        self.textEdit.setText("\nLlave publica seleccionada: "+public_key)
    
    

    def enviar(self):
        a=connect1.connect1()
        chunk_size = 470
        offset = 0
        end_loop = False
        encrypted =  ""
        self.textEdit.setText("\nCargando...")
        if not self.textEdit_2.toPlainText() or not self.textEdit_3.toPlainText() or not self.textEdit_4.toPlainText():
            self.textEdit.setText("Los campos con una * son obligatorios, favor de llenarlos")
        else:
            x=self.textEdit_4.toPlainText()
            try:

                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                    s.connect((HOST, PORT))
                    print(1)
                    fd = open(self.textEdit_2.toPlainText(), "rb")
                    llave = fd.read()
                    fd.close()
                    print(2)
                    fd = open(self.textEdit_3.toPlainText(), "rb")
                    llave2 = fd.read()
                    fd.close()
                    print(3)
                    encrypted_blob=a.encrypt_blob(str.encode(x),llave)
                    d=a.sign_data(llave2,str.encode(x))
                    print(4)
                    firma=d.decode(encoding='UTF-8',errors='strict')
                    encripcion=encrypted_blob.decode(encoding='UTF-8',errors='strict')
                    llavePublica=llave.decode(encoding='UTF-8',errors='strict')
                    llavePrivada=llave2.decode(encoding='UTF-8',errors='strict')
                    y={
                        "firma": firma,
                        "encripcion": encripcion,
                        "llavePublica":llavePublica,
                        "llavePrivada":llavePrivada
                    }
                    print(5)
                    try:
                        p=json.load(y)
                        print(p)
                    except:
                        print("error")    
                    print(6)
                    asd=json.dumps(y)
                    print
                    v=pickle.dumps(asd)
                    print(7)
                    s.sendall(v)
                    print(8)
                    data = s.recv(4096)
            except:
                self.textEdit.setText("Servidor Inactivo, intentelo de nuevo")
                
            else:
                dato=pickle.loads(data)
                
                dato=pickle.loads(data)
                datojson=json.loads(dato)
                desencriptado=a.decrypt_blob(datojson["encripcion"].encode(),datojson["llavePrivada"].encode())
                desencriptado=desencriptado.decode(encoding='UTF-8',errors='strict')
                verdad=a.verify_sign(datojson["llavePublica"].encode(),datojson["firma"].encode(),desencriptado.encode())
                if verdad==False:
                    print("Falso")
                self.textEdit.setText("\nDato Recibido: "+ str(desencriptado))
                self.textEdit_5.setText(str(desencriptado))

        

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
